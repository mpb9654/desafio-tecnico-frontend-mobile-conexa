export const CONSULTA = 'consulta';
export const CONSULTA_ID = (id: number) => `consulta/${id}`;
export const CONSULTAS = 'consultas';
