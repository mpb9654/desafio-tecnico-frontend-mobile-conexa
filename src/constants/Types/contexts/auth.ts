import { ReactNode } from 'react';

export type AuthState = {
  acessToken: string;
  username: string;
};

export type SignInCredentials = {
  username: string;
  password: string;
};

export type AppProviderProps = {
  children: ReactNode;
};
