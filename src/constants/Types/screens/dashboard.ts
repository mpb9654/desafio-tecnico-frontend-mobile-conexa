type MedicoTypes = {
  email: null;
  id: number;
  nome: string;
};

export type DataConsultaTypes = {
  dataConsulta: string;
  id: number;
  medico: MedicoTypes;
  observacao: string;
  paciente: string;
};
