import { useAuth } from '@hooks/useAuth';
import React from 'react';
import { AuthRoutes } from './app.auth.routes';
import { StackRoutes } from './app.stack.routes';

export const Routes = () => {
  const { data } = useAuth();

  return data.username ? <StackRoutes /> : <AuthRoutes />;
};
