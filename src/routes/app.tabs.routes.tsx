import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import colors from '../styles/theme';
import { MaterialIcons } from '@expo/vector-icons';
import { Dashboard } from '@screens/Dashboard';
import { AddConsulta } from '@screens/AddConsulta';

const { Navigator, Screen } = createBottomTabNavigator();

export const TabsRoutes = () => {
  return (
    <Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: colors.colors.primary,
        tabBarInactiveTintColor: colors.colors.text_tertiary,
        tabBarLabelPosition: 'beside-icon',
        tabBarStyle: {
          paddingVertical: Platform.OS === 'ios' ? 20 : 0,
          height: 64,
        },
      }}
    >
      <Screen
        name="Minhas Consultas"
        component={Dashboard}
        options={{
          tabBarIcon: ({ size, color }) => (
            <MaterialIcons
              name="format-list-bulleted"
              size={size}
              color={color}
            />
          ),
        }}
      />

      <Screen
        name="Nova Consulta"
        component={AddConsulta}
        options={{
          tabBarIcon: ({ size, color }) => (
            <MaterialIcons
              name="add-circle-outline"
              size={size}
              color={color}
            />
          ),
        }}
      />
    </Navigator>
  );
};
