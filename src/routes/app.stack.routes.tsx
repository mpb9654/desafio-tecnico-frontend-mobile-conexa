import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { TabsRoutes } from './app.tabs.routes';

import { Details } from '@screens/Details';

const { Navigator, Screen } = createNativeStackNavigator();

export const StackRoutes = () => (
  <Navigator
    initialRouteName="TabsRoutes"
    screenOptions={{
      headerShown: false,
    }}
  >
    <Screen name="TabsRoutes" component={TabsRoutes} />
    <Screen name="Details" component={Details} />
  </Navigator>
);
