import React from 'react';
import { AppProviderProps } from '@constants/Types/contexts/auth';

import { AuthContextProvider } from './AuthContext';

export const AppProvider = ({ children }: AppProviderProps) => (
  <AuthContextProvider>{children}</AuthContextProvider>
);
