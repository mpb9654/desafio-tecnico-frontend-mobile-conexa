import React, {
  createContext,
  useCallback,
  useState,
  useEffect,
  useMemo,
} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { api } from '../services/api';
import { LOGIN } from '../constants/endpoints/login';
import {
  AuthState,
  SignInCredentials,
} from '@src/constants/Types/contexts/auth';

interface AuthContextData {
  data: AuthState;
  username: string;
  loading: boolean;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut: () => void;
}

export const AuthContext = createContext<AuthContextData>(
  {} as AuthContextData,
);

export const AuthContextProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>({} as AuthState);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadStoragedData(): Promise<void> {
      const [acessToken, username] = await AsyncStorage.multiGet([
        '@ConexaApp:acessToken',
        '@ConexaApp:username',
      ]);

      if (acessToken[1] && username[1]) {
        api.defaults.headers.Authorization = `Bearer ${acessToken[1]}`;

        setData({
          acessToken: acessToken[1],
          username: username[1],
        });
      }
      setLoading(false);
    }

    loadStoragedData();
  }, []);

  const signIn = useCallback(async ({ email, password }) => {
    try {
      const { data } = await api.post(LOGIN, {
        email,
        password,
      });

      const { token, nome } = data.data;

      setData({ acessToken: token, username: nome });

      await AsyncStorage.multiSet([
        ['@ConexaApp:acessToken', token],
        ['@ConexaApp:username', nome],
      ]);

      api.defaults.headers.authorization = `Bearer ${token}`;

      setLoading(false);
    } catch (error) {
      console.log('algo errado');
    }
  }, []);

  const signOut = useCallback(async () => {
    await AsyncStorage.multiRemove([
      '@ConexaApp:acessToken',
      '@ConexaApp:username',
    ]);
    setData({} as AuthState);
  }, []);

  const values = useMemo(
    () => ({
      data,
      loading,
      signIn,
      username: data.username,
      signOut,
    }),
    [signOut, data],
  );

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
};
