import styled from 'styled-components/native';
import { FontAwesome5, Feather } from '@expo/vector-icons';

export const Container = styled.View`
  width: 100%;
  border: 0.3px;
  border-color: ${({ theme }) => theme.colors.border};
  border-radius: 12px;
  background-color: ${({ theme }) => theme.colors.shape};
  padding: 18px 12px;
  margin-top: 36px;
`;
export const Flex = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: space-between;
`;
export const Avatar = styled.View`
  width: 25%;
`;
export const Info = styled.View`
  width: 65%;
`;
export const Out = styled.View`
  width: 10%;
`;
export const Title = styled.Text`
  font-size: 21px;
  font-weight: 700;
`;
export const SubTitle = styled.Text``;
export const BtnLogOut = styled.TouchableOpacity``;
export const Icon = styled(FontAwesome5)`
  color: ${({ theme }) => theme.colors.primary};
`;
export const Icon1 = styled(Feather)`
  color: ${({ theme }) => theme.colors.erro};
`;
