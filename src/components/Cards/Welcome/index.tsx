import React from 'react';
import { useAuth } from '@hooks/useAuth';

import * as S from './styles';

export const Welcome = () => {
  const { data, signOut } = useAuth();

  return (
    <S.Container>
      <S.Flex>
        <S.Avatar>
          <S.Icon name="user-md" size={56} />
        </S.Avatar>
        <S.Info>
          <S.SubTitle>Bem Vindo,</S.SubTitle>
          <S.Title>{data.username}</S.Title>
        </S.Info>
        <S.Out>
          <S.BtnLogOut onPress={signOut}>
            <S.Icon1 name="log-out" size={24} />
          </S.BtnLogOut>
        </S.Out>
      </S.Flex>
    </S.Container>
  );
};
