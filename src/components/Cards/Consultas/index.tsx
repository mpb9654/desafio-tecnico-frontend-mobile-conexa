import React, { useState } from 'react';

import * as S from './styles';

import { api } from '@services/api';
import { CONSULTA_ID } from '../../../constants/endpoints/consulta';
import { useNavigation } from '@react-navigation/native';
import { DataConsultaTypes } from '@constants/Types/screens/dashboard';

type ConsultasTypes = {
  item: DataConsultaTypes;
};

export const Consultas = ({ item }: ConsultasTypes) => {
  const navigation = useNavigation();

  const handleDetailsConsulta = (id: number) => {
    navigation.navigate('Details', { id });
  };

  return (
    <S.Container>
      <S.Flex>
        <S.Avatar>
          <S.Icon2 name="hospital-user" size={36} />
        </S.Avatar>
        <S.Info>
          <S.Title>{item.paciente}</S.Title>
          <S.InfoTimer>
            <S.Icon name="md-alarm-outline" size={16} />
            <S.SubTitle>{`${item.dataConsulta.split(' ')[0]} às ${
              item.dataConsulta.split(' ')[1]
            }`}</S.SubTitle>
          </S.InfoTimer>
        </S.Info>
        <S.Out>
          <S.BtnArrow onPress={() => handleDetailsConsulta(item.id)}>
            <S.Icon1 name="arrow-forward-ios" size={24} />
          </S.BtnArrow>
        </S.Out>
      </S.Flex>
    </S.Container>
  );
};
