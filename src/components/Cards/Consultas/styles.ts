import styled from 'styled-components/native';
import { Ionicons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';

export const Container = styled.View`
  width: 100%;
  padding: 0 12px;
  margin-top: 12px;
  margin-bottom: 12px;
`;
export const Flex = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  border: 0.3px;
  border-color: ${({ theme }) => theme.colors.border};
  border-radius: 12px;
  background-color: ${({ theme }) => theme.colors.shape};
  padding: 24px 12px;
`;
export const Avatar = styled.View`
  width: 13%;
`;
export const Info = styled.View`
  width: 65%;
`;
export const InfoTimer = styled.View`
  flex-direction: row;
`;
export const Out = styled.View`
  width: 10%;
`;
export const Title = styled.Text`
  font-size: 18px;
  font-weight: 700;
`;
export const SubTitle = styled.Text`
  margin-left: 8px;
`;
export const BtnArrow = styled.TouchableOpacity`
  margin-left: 8px;
`;
export const Icon = styled(Ionicons)`
  color: ${({ theme }) => theme.colors.success};
`;
export const Icon1 = styled(MaterialIcons)`
  color: ${({ theme }) => theme.colors.border};
`;
export const Icon2 = styled(FontAwesome5)`
  color: ${({ theme }) => theme.colors.erro};
`;
