import { TextInput } from 'react-native';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';
import styled, { css } from 'styled-components/native';

type Props = {
  isFocused?: boolean;
  isFilled?: boolean;
  isEnaled?: boolean;
  isErrored?: boolean;
  type?: string;
};

export const Container = styled.View`
  width: 100%;
  margin-top: 16px;
`;
export const Title = styled.Text`
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text_tertiary};
  margin-bottom: ${RFPercentage(1)}px;
`;
export const InputDate = styled.View`
  flex-direction: row;
  align-items: center;
  border: 1px;
  border-color: ${({ theme }) => theme.colors.border};

  padding: ${RFPercentage(2)}px 4px;
`;
export const InputText = styled(TextInput)<Props>`
  flex: 1;
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text_secondary};

  ${({ isFocused }) =>
    isFocused &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}
`;
export const ButtonDate = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  flex: 1;
  background-color: transparent;
`;
export const TextDate = styled.Text`
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text_tertiary};
`;
