import React, { useEffect, useState } from 'react';
import { format } from 'date-fns';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

import * as S from './styles';

type InputTextProps = {
  title: string;
  value: string;
  type: string;
  testID?: string;
  setState(value: string): void;
};

export const DateText = ({
  title,
  testID,
  value,
  type,
  setState,
}: InputTextProps) => {
  const [textDate, setTextDate] = useState('');
  const [isShowDatePicker, setIsShowDatePicker] = useState(false);

  useEffect(() => {
    if (value === '') {
      setTextDate(type === 'date' ? 'dd/mm/aaaa' : 'HH:mm');
    } else {
      format(new Date(value), type === 'date' ? 'dd/MM/yyyy' : 'HH:mm');
    }
  }, [value]);

  const handleChangeTime = (dateTime: Date | undefined) => {
    if (dateTime) {
      setState(dateTime.toISOString());
      setTextDate(format(dateTime, type === 'date' ? 'dd/MM/yyyy' : 'HH:mm'));
    }
    setIsShowDatePicker(false);
  };

  const showDatePicker = () => {
    setIsShowDatePicker(true);
  };

  const hideDatePicker = () => {
    setIsShowDatePicker(false);
  };

  return (
    <S.Container>
      <S.Title>{title}</S.Title>
      <S.InputDate>
        <S.ButtonDate onPress={showDatePicker}>
          <S.TextDate>{textDate}</S.TextDate>
        </S.ButtonDate>
        <DateTimePickerModal
          isVisible={isShowDatePicker}
          mode={type === 'date' ? 'date' : 'time'}
          onConfirm={handleChangeTime}
          onCancel={hideDatePicker}
        />
      </S.InputDate>
    </S.Container>
  );
};
