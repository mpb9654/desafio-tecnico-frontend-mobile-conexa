import React, { useState, useCallback, useEffect } from 'react';
import { TextInputProps } from 'react-native';
import { Control, Controller } from 'react-hook-form';

import * as S from './styles';

interface InputCustomerProps extends TextInputProps {
  iconName: string;
  control: Control;
  name: string;
  error: string;
  secureTextEntry?: boolean;
}

export const InputCustomer = ({
  iconName,
  control,
  name,
  error,
  secureTextEntry = false,
  ...rest
}: InputCustomerProps) => {
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);
  const [isPasswordVisible, setIsPasswordVisible] = useState(true);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(value => {
    setIsFocused(false);

    setIsFilled(!!value);
  }, []);

  function handlePasswordVisibilityChange() {
    setIsPasswordVisible(prevState => !prevState);
  }

  return (
    <>
      <S.Container
        isErrored={!!error}
        isFocused={isFocused}
        testID="input-container"
      >
        <Controller
          control={control}
          name={name}
          render={({ field: { onChange, value } }) => (
            <>
              {secureTextEntry ? (
                <S.Icon name="lock" size={24} />
              ) : (
                <S.Icon1 name="email" size={24} />
              )}
              <S.InputText
                onFocus={handleInputFocus}
                onBlur={() => handleInputBlur(value)}
                value={value}
                onChangeText={onChange}
                underlineColorAndroid="transparent"
                placeholderTextColor="#918E8A"
                secureTextEntry={secureTextEntry ? isPasswordVisible : false}
                {...rest}
              />
              {secureTextEntry ? (
                <S.BtnPassword onPress={handlePasswordVisibilityChange}>
                  {isPasswordVisible ? (
                    <S.Icon name="eye-slash" size={24} />
                  ) : (
                    <S.Icon name="eye" size={24} />
                  )}
                </S.BtnPassword>
              ) : null}
            </>
          )}
        />
      </S.Container>
      {error && <S.Error>{error}</S.Error>}
    </>
  );
};
