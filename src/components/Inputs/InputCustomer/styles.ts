import styled, { css } from 'styled-components/native';
import { TextInput } from 'react-native';
import { FontAwesome5, MaterialIcons } from '@expo/vector-icons';

interface TypeError {
  isErrored: boolean;
  isFocused: boolean;
  isFilled?: boolean;
}

export const Container = styled.View<TypeError>`
  /* height: 45px; */
  width: 100%;
  margin-top: 16px;
  padding: 12px;
  background: ${({ theme }) => theme.colors.background_inputs};
  flex-direction: row;
  align-items: center;
  border-radius: 6px;
  border: 0.3px;
  border-color: ${({ theme }) => theme.colors.background_inputs};

  ${({ isErrored }) =>
    isErrored &&
    css`
      border-color: ${({ theme }) => theme.colors.erro};
    `}
  ${({ isFocused }) =>
    isFocused &&
    css`
      border-color: ${({ theme }) => theme.colors.primary};
    `}
`;
export const InputText = styled(TextInput)`
  flex: 1;
  color: ${({ theme }) => theme.colors.text};
  margin-left: 8px;
`;
export const Error = styled.Text`
  color: ${({ theme }) => theme.colors.erro};
`;
export const IconContainer = styled.View`
  height: 43px;
  width: 55px;
  justify-content: center;
  align-items: center;
  margin-right: 4px;
  background-color: transparent;
`;
export const BtnPassword = styled.TouchableOpacity``;
export const Icon = styled(FontAwesome5)`
  color: ${({ theme }) => theme.colors.border};
`;
export const Icon1 = styled(MaterialIcons)`
  color: ${({ theme }) => theme.colors.border};
`;
