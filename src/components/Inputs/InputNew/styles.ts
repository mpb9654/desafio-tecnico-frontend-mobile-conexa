import { TextInput } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import styled, { css } from 'styled-components/native';

type Props = {
  isFocused?: boolean;
  type?: string;
};

export const Container = styled.View`
  width: 100%;
  margin-top: 8px;
`;
export const Title = styled.Text<Props>`
  color: ${({ theme }) => theme.colors.text_tertiary};
  margin-bottom: 4px;
  font-size: ${RFValue(14)}px;

  ${({ isFocused }) =>
    isFocused &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}
`;
export const InputWrapper = styled.View<Props>`
  border: 1px;
  border-color: ${({ theme }) => theme.colors.border};
  border-radius: 2px;
  width: 100%;
  height: ${RFValue(48)}px;
  padding: 4px 16px;

  ${({ isFocused }) =>
    isFocused &&
    css`
      border-color: ${({ theme }) => theme.colors.primary};
    `}

  ${({ type }) =>
    type === 'description' &&
    css`
      height: ${RFValue(200)}px;
    `}
`;
export const InputText = styled(TextInput)<Props>`
  flex: 1;
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text};

  ${({ isFocused }) =>
    isFocused &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}
`;
