import React, { useState } from 'react';
import { Control, Controller } from 'react-hook-form';

import * as S from './styles';
import { TextInputProps } from 'react-native';

interface InputTextProps extends TextInputProps {
  title: string;
  type: string;
  control: Control;
  name: string;
}

export const InputNew = ({
  control,
  name,
  type,
  title,
  ...rest
}: InputTextProps) => {
  const [isFocused, setIsFocused] = useState(false);

  function handleInputFocus() {
    setIsFocused(true);
  }

  function handleInputBlur() {
    setIsFocused(false);
  }

  return (
    <S.Container>
      <Controller
        control={control}
        name={name}
        render={({ field: { onChange, value } }) => (
          <>
            <S.Title isFocused={isFocused}>{title}</S.Title>
            <S.InputWrapper isFocused={isFocused} type={type}>
              {type === 'title' && (
                <S.InputText
                  value={value}
                  onChangeText={onChange}
                  isFocused={isFocused}
                  onFocus={handleInputFocus}
                  placeholderTextColor="#918E8A"
                  onBlur={handleInputBlur}
                  {...rest}
                />
              )}
              {type === 'description' && (
                <S.InputText
                  value={value}
                  onChangeText={onChange}
                  style={{ textAlignVertical: 'top' }}
                  multiline
                  numberOfLines={10}
                  isFocused={isFocused}
                  onFocus={handleInputFocus}
                  placeholderTextColor="#918E8A"
                  onBlur={handleInputBlur}
                  {...rest}
                />
              )}
            </S.InputWrapper>
          </>
        )}
      />
    </S.Container>
  );
};
