import styled, { css } from 'styled-components/native';
import { TouchableOpacity } from 'react-native';

export const Container = styled(TouchableOpacity)`
  width: 90%;
  background: ${({ theme }) => theme.colors.primary};
  align-items: center;
  justify-content: center;
  border-radius: 12px;
  margin-top: 12px;
  padding: 12px 21px;
`;
export const Title = styled.Text`
  font-weight: 700;
  font-size: 18px;
  color: ${({ theme }) => theme.colors.shape};
  text-transform: uppercase;
`;
