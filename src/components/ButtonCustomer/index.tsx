import React from 'react';
import { ActivityIndicator, TouchableOpacityProps } from 'react-native';

import * as S from './styles';

interface ButtonCustomerProps extends TouchableOpacityProps {
  title: string;
  loading?: boolean;
  onPress: () => void;
}

export const ButtonCustomer = ({
  title,
  onPress,
  loading = false,
  ...rest
}: ButtonCustomerProps) => {
  return (
    <S.Container onPress={onPress} {...rest}>
      <S.Title>{title}</S.Title>
      {loading && <ActivityIndicator size="small" color="white" />}
    </S.Container>
  );
};
