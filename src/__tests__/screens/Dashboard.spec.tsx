import React from 'react';
import { render, waitFor, act } from '@testing-library/react-native';
import theme from '../../styles/theme';
import AxiosMock from 'axios-mock-adapter';
import { ThemeProvider } from 'styled-components/native';

import { Dashboard } from '@screens/Dashboard';
import { api } from '@services/api';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

const apiMock = new AxiosMock(api);
const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
}));

jest.mock('../../hooks/useAuth.tsx', () => ({
  __esModule: true,
  useAuth: jest.fn().mockReturnValue({
    data: {
      username: 'Dr. Daniel Vieira',
    },
  }),
}));

describe('Screen Dashboard', () => {
  it('should componets in screen Dashboard', () => {
    const { getByText, getAllByPlaceholderText } = render(<Dashboard />, {
      wrapper: ProvidersWrapper,
    });

    expect(getByText(/No total temos/)).toBeTruthy();
  });
});

describe('Screen Dashboard data', () => {
  it('should list correct values', async () => {
    apiMock.onGet('consultas').reply(200, [
      {
        dataConsulta: '2020-04-23 08:30',
        id: 16,
        medico: {
          email: null,
          id: 1,
          nome: 'Dr. Daniel Vieira',
        },
        observacao: 'Exemplo de consulta',
        paciente: 'Diego Senna',
      },
    ]);

    const { getByText } = render(<Dashboard />, {
      wrapper: ProvidersWrapper,
    });

    waitFor(() => expect(getByText('Diego Senna')).toBeTruthy(), {
      timeout: 200,
    });
  });
});
