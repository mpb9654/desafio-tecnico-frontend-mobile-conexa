import React from 'react';
import { render, fireEvent, act, waitFor } from '@testing-library/react-native';
// import { ProvidersWrapper } from '../../utils/jest-utils/wrapper';
import theme from '../../styles/theme';
// import { Controller, useForm } from 'react-hook-form';
// import { Button, TextInput, View, Text } from 'react-native';
import { ThemeProvider } from 'styled-components/native';
import MockAdapter from 'axios-mock-adapter';

import { AddConsulta } from '@screens/AddConsulta';
import { api } from '@services/api';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);
const mockedHistoryPush = jest.fn();
const mockedNavigate = jest.fn();
const apiMock = new MockAdapter(api);

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
}));

jest.mock('../../hooks/useAuth.tsx', () => ({
  __esModule: true,
  useAuth: jest.fn().mockReturnValue({
    signIn: jest.fn(),
  }),
}));

jest.mock('date-fns', () => ({
  __esModule: true,
  format: jest.fn(),
}));

describe('Screen AddConsulta', () => {
  it('should componets in screen AddConsulta', () => {
    const { getByText } = render(<AddConsulta />, {
      wrapper: ProvidersWrapper,
    });

    expect(getByText('Dados para cadastrar nova consulta')).toBeTruthy();
  });
});

describe('Screen AddConsulta', () => {
  it('should send correct values', async () => {
    const handleSaveConsult = jest.fn();
    const { getByTestId } = render(<AddConsulta />, {
      wrapper: ProvidersWrapper,
    });

    const apiResponse = {
      dataConsulta: '2022-03-07T12:08:41.687Z',
      idMedico: 0,
      paciente: 'Joe',
      observacao: 'observacao',
    };

    apiMock.onPost('consulta').reply(200, apiResponse);

    fireEvent.press(getByTestId('Enviar'));

    waitFor(() => {
      expect(mockedHistoryPush).toHaveBeenCalledWith('Dashboard');
    });
  });
});
