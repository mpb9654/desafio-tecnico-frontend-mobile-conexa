import React from 'react';
import { render, fireEvent, act, waitFor } from '@testing-library/react-native';
import theme from '../../styles/theme';
import { ThemeProvider } from 'styled-components/native';

import { SignIn } from '@screens/SignIn';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
}));

jest.mock('../../hooks/useAuth.tsx', () => ({
  __esModule: true,
  useAuth: jest.fn().mockReturnValue({
    signIn: jest.fn(),
  }),
}));

describe('Screen SignIn', () => {
  it('should componets in screen SignIn', () => {
    const { getByText, getAllByPlaceholderText } = render(<SignIn />, {
      wrapper: ProvidersWrapper,
    });

    expect(getByText('Faça seu login')).toBeTruthy();
    expect(getAllByPlaceholderText('Seu e-mail')).toBeTruthy();
    expect(getAllByPlaceholderText('Sua senha')).toBeTruthy();
    expect(getByText('Entrar')).toBeTruthy();
  });
});

describe('Screen SignIn', () => {
  it('should not trigger error for correct values', async () => {
    const { getByTestId, queryByTestId } = render(<SignIn />, {
      wrapper: ProvidersWrapper,
    });

    fireEvent.changeText(
      getByTestId('emailInput'),
      'igor.silva@conexasaude.com.br',
    );
    fireEvent.changeText(getByTestId('passwordInput'), '12345678');

    fireEvent.press(getByTestId('Entrar'));
    expect(queryByTestId('nameErrorText')).not.toBeTruthy();
  });

  it('should trigger error for empty input', async () => {
    const { getByTestId, queryByTestId } = render(<SignIn />, {
      wrapper: ProvidersWrapper,
    });

    fireEvent.press(getByTestId('Entrar'));

    waitFor(() => expect(queryByTestId('nameErrorText')).toBeTruthy(), {
      timeout: 200,
    });
  });
});
