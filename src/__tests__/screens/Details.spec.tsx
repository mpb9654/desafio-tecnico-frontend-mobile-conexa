import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import theme from '../../styles/theme';
import { ThemeProvider } from 'styled-components/native';
import { Details } from '@screens/Details';
import { api } from '@services/api';
import AxiosMock from 'axios-mock-adapter';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);
const apiMock = new AxiosMock(api);
const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
  useRoute: jest.fn().mockReturnValue({
    routes: jest.fn(),
    params: [{ id: 16 }],
  }),
}));

jest.mock('../../hooks/useAuth.tsx', () => ({
  __esModule: true,
  useAuth: jest.fn().mockReturnValue({
    signIn: jest.fn(),
  }),
}));

describe('Screen Details', () => {
  it('should componets in screen Details', async () => {
    const { getByTestId } = render(<Details />, {
      wrapper: ProvidersWrapper,
    });

    expect(getByTestId('logo')).toBeTruthy();
  });

  it('should componets in screen Details', async () => {
    apiMock.onGet('consultas').reply(200, [
      {
        dataConsulta: '2020-04-23 08:30',
        id: 16,
        medico: {
          email: null,
          id: 1,
          nome: 'Dr. Daniel Vieira',
        },
        observacao: 'Exemplo de consulta',
        paciente: 'Diego Senna',
      },
    ]);
    const { getByText, getByTestId } = render(<Details />, {
      wrapper: ProvidersWrapper,
    });

    waitFor(() => expect(getByText('Diego Senna')).toBeTruthy(), {
      timeout: 200,
    });
  });
});
