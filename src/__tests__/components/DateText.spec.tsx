import React from 'react';
import { render } from '@testing-library/react-native';
import { ThemeProvider } from 'styled-components/native';
import theme from '../../styles/theme';

import { DateText } from '@components/Inputs/InputDateTime';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
}));

describe('DateText Component', () => {
  it('Should render to date', () => {
    const { getByText } = render(
      <DateText
        title="Data da Consulta"
        type="date"
        value="2022-03-07T04:10:53.359Z"
        setState={jest.fn()}
      />,
      {
        wrapper: ProvidersWrapper,
      },
    );

    expect(getByText('Data da Consulta')).toBeTruthy();
  });

  it('Should render to time', () => {
    const { getByText } = render(
      <DateText
        title="Horário da Consulta"
        type="time"
        value="2022-03-07T04:10:53.359Z"
        setState={jest.fn()}
      />,
      {
        wrapper: ProvidersWrapper,
      },
    );

    expect(getByText('Horário da Consulta')).toBeTruthy();
  });
});
