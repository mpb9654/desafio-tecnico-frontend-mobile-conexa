import React from 'react';

import { render } from '@testing-library/react-native';
import { ThemeProvider } from 'styled-components/native';
import theme from '../../styles/theme';

import { Welcome } from '@components/Cards/Welcome';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
}));

jest.mock('../../hooks/useAuth.tsx', () => ({
  __esModule: true,
  useAuth: jest.fn().mockReturnValue({
    data: [{ username: 'Dr. Daniel Vieira' }],
  }),
}));

describe('Welcome Component', () => {
  it('Should render', () => {
    const { getByText } = render(<Welcome />, {
      wrapper: ProvidersWrapper,
    });

    expect(getByText('Bem Vindo,')).toBeTruthy();
  });
});
