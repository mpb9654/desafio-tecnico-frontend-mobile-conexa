import React from 'react';
import { render } from '@testing-library/react-native';
import { ThemeProvider } from 'styled-components/native';
import theme from '../../styles/theme';

import { ButtonCustomer } from '@components/ButtonCustomer';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
}));

describe('ButtonCusomer Component', () => {
  it('Should render', () => {
    const { getByText } = render(
      <ButtonCustomer title="Entrar" onPress={jest.fn()} />,
      {
        wrapper: ProvidersWrapper,
      },
    );

    expect(getByText('Entrar')).toBeTruthy();
  });
});
