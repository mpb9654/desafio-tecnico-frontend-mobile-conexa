import React from 'react';
import { render } from '@testing-library/react-native';
import { ThemeProvider } from 'styled-components/native';
import theme from '../../styles/theme';

import { Consultas } from '@components/Cards/Consultas';

const ProvidersWrapper: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: mockedNavigate,
  }),
}));

const response = {
  dataConsulta: '2020-04-23 08:30',
  id: 16,
  medico: {
    email: null,
    id: 1,
    nome: 'Dr. Daniel Vieira',
  },
  observacao: 'Exemplo de consulta',
  paciente: 'Diego Senna',
};

describe('Consultas Component', () => {
  it('Should render', () => {
    const { getByText } = render(<Consultas item={response} />, {
      wrapper: ProvidersWrapper,
    });

    expect(getByText('Diego Senna')).toBeTruthy();
  });
});
