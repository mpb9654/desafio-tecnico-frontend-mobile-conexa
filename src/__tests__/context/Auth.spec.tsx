import { useAuth } from '@hooks/useAuth';
import { AppProvider } from '../../context';
import { renderHook, act } from '@testing-library/react-hooks';
import AsyncStorage from '@react-native-async-storage/async-storage';

import MockAdapter from 'axios-mock-adapter';

import { api } from '../../services/api';

const apiMock = new MockAdapter(api);

jest.mock('../../hooks/useAuth.tsx', () => ({
  __esModule: true,
  useAuth: jest.fn().mockReturnValue({
    signIn: jest.fn(),
    signOut: jest.fn(),
    username: 'Dr. Daniel Vieira',
  }),
}));

jest.mock('@react-native-async-storage/async-storage', () => ({
  __esModule: true,
  default: {
    setItem: jest.fn(),
    removeItem: jest.fn(),
    getItem: jest.fn().mockReturnValue(null),
  },
}));

const apiResponse = {
  data: {
    email: null,
    nome: 'Dr. Daniel Vieira',
    token: 'token-123',
  },
};
describe('Auth hook', () => {
  it('should be able to sign in', async () => {
    apiMock.onPost('login').reply(200, apiResponse);

    const { result, waitForNextUpdate } = renderHook(() => useAuth(), {
      wrapper: AppProvider,
    });

    result.current.signIn({
      username: 'igor.silva@conexasaude.com.br',
      password: '12345678',
    });

    await AsyncStorage.setItem('@ConexaApp:acessToken', apiResponse.data.token);
    await AsyncStorage.setItem('@ConexaApp:username', apiResponse.data.nome);

    expect(result.current.username).toEqual('Dr. Daniel Vieira');
  });

  it('should restore saved data from storage when auth inits', async () => {
    await AsyncStorage.setItem('@ConexaApp:username', apiResponse.data.nome);

    const username = await AsyncStorage.getItem('@ConexaApp:username');

    const { result } = renderHook(() => useAuth(), {
      wrapper: AppProvider,
    });

    expect(username).not.toBeTruthy();
  });

  it('should be able to sign out', async () => {
    await AsyncStorage.setItem('@ConexaApp:username', apiResponse.data.nome);

    let username = await AsyncStorage.getItem('@ConexaApp:username');

    await AsyncStorage.removeItem('@ConexaApp:username');

    const { result } = renderHook(() => useAuth(), {
      wrapper: AppProvider,
    });

    act(() => {
      result.current.signOut;
    });

    expect(username).not.toBeTruthy();
  });
});
