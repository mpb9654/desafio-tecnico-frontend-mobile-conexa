export default {
  colors: {
    primary: '#3CB4E7',
    secondary: '#EF4056',

    text: '#494848',
    text_secondary: '#8D8985',
    text_tertiary: '#918E8A',

    shape: '#FFFFFF',
    background_primary: '#FFFFFF',
    background_inputs: '#F6F4F3',

    border: '#D2CDCA',

    success: '#39E335',
    pendente: '#C69214',
    erro: '#CE543A',
  },
  fonts: {
    primary_400: 'Roboto_400Regular',
    primary_500: 'Roboto_500Medium',
    primary_700: 'Roboto_700Bold',
  },
};
