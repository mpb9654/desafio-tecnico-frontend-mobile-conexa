import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Platform, ToastAndroid, View } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';

import { api } from '@services/api';
import { CONSULTA_ID } from '@constants/endpoints/consulta';
import { RoutesTypes } from '@constants/Types/screens/details';
import { DataConsultaTypes } from '@constants/Types/screens/dashboard';

import logo from '@assets/img/logo-conexa.png';

import * as S from './styles';

export const Details = () => {
  const routes = useRoute();
  const navigation = useNavigation();
  const { id } = routes.params as RoutesTypes;
  const [details, setDetails] = useState<DataConsultaTypes>(
    {} as DataConsultaTypes,
  );
  const [isloading, setIsLoading] = useState(true);

  useEffect(() => {
    const loadDetails = async () => {
      try {
        const { data } = await api.get(CONSULTA_ID(id));
        setDetails(data.data);
        setIsLoading(false);
      } catch {
        if (Platform.OS == 'android') {
          ToastAndroid.show(
            'Erro interno. Tente novamente.',
            ToastAndroid.LONG,
          );
        }
      }
    };
    loadDetails();
  }, []);

  return (
    <S.Container>
      <S.TopContainer>
        <S.BtnBack onPress={() => navigation.goBack()}>
          <S.Icon1 name="arrow-back-ios" size={21} />
        </S.BtnBack>
        <S.ImgTop source={logo} testID="logo" />
      </S.TopContainer>
      {isloading ? (
        <ActivityIndicator size="large" color="#3CB4E7" />
      ) : (
        <>
          <S.Title>Detalhes da consulta - {details.id}</S.Title>
          <S.ContainerConsulta>
            <S.InfoTitle>
              <S.Icon name="user-md" size={21} />
              <View style={{ marginLeft: 8 }}>
                <S.Info>Nome do Médico</S.Info>
                <S.Description>{details.medico.nome}</S.Description>
              </View>
            </S.InfoTitle>
            <S.InfoTitle>
              <S.Icon name="user-injured" size={21} />
              <View style={{ marginLeft: 8 }}>
                <S.Info>Nome do Paciente</S.Info>
                <S.Description>{details.paciente}</S.Description>
              </View>
            </S.InfoTitle>
            <S.InfoTitle>
              <S.Icon name="calendar" size={21} />
              <View style={{ marginLeft: 8 }}>
                <S.Info>Data consulta</S.Info>
                <S.Description>
                  {details.dataConsulta.split(' ')[0]}
                </S.Description>
              </View>
            </S.InfoTitle>
            <S.InfoTitle>
              <S.Icon name="clock" size={21} />
              <View style={{ marginLeft: 8 }}>
                <S.Info>Horário consulta</S.Info>
                <S.Description>
                  {details.dataConsulta.split(' ')[1]}
                </S.Description>
              </View>
            </S.InfoTitle>
            <S.InfoTitle>
              <S.Icon name="clipboard-list" size={21} />
              <View style={{ marginLeft: 8 }}>
                <S.Info>Observação</S.Info>
                <S.Description>{details.observacao}</S.Description>
              </View>
            </S.InfoTitle>
          </S.ContainerConsulta>
        </>
      )}
    </S.Container>
  );
};
