import styled from 'styled-components/native';
import { MaterialIcons, FontAwesome5 } from '@expo/vector-icons';

export const Container = styled.View`
  flex: 1;
  padding: 24px;
  background: ${({ theme }) => theme.colors.background_primary};
`;
export const ContainerConsulta = styled.View`
  width: 100%;
  padding: 24px;
  margin-top: 24px;
  border: 0.3px;
  border-color: ${({ theme }) => theme.colors.border};
  border-radius: 12px;
`;
export const Title = styled.Text`
  font-size: 16px;
  margin-left: 24px;
  margin-top: 24px;
  font-weight: 700;
`;
export const InfoTitle = styled.View`
  margin: 16px 0;
  flex-direction: row;
`;
export const Info = styled.Text`
  font-size: 15px;
`;
export const Description = styled.Text`
  font-size: 18px;
  font-weight: 700;
`;
export const TopContainer = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-top: 24px;
  position: relative;
`;
export const ImgTop = styled.Image`
  height: 36px;
  width: 190px;
`;
export const BtnBack = styled.TouchableOpacity`
  position: absolute;
  left: 0;
  top: 8px;
`;
export const Icon = styled(FontAwesome5)`
  color: ${({ theme }) => theme.colors.border};
`;
export const Icon1 = styled(MaterialIcons)`
  color: ${({ theme }) => theme.colors.border};
`;
