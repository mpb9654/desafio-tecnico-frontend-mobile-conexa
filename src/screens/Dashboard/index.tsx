import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Platform, ToastAndroid } from 'react-native';

import { Welcome } from '@components/Cards/Welcome';
import { Consultas } from '@components/Cards/Consultas';
import { DataConsultaTypes } from '@constants/Types/screens/dashboard';
import { CONSULTAS } from '@constants/endpoints/consulta';
import { api } from '@services/api';

import logo from '@assets/img/logo-conexa.png';

import * as S from './styles';

export const Dashboard = () => {
  const [data, setData] = useState<DataConsultaTypes[]>([]);
  const [isloading, setIsLoading] = useState(true);

  useEffect(() => {
    const loadConsultas = async () => {
      try {
        const { data } = await api.get(CONSULTAS);
        setData(data.data);
        setIsLoading(false);
      } catch {
        if (Platform.OS == 'android') {
          ToastAndroid.show(
            'Erro interno. Tente novamente.',
            ToastAndroid.LONG,
          );
        }
      } finally {
        setIsLoading(false);
      }
    };
    loadConsultas();
  }, []);

  return (
    <S.Container>
      <S.TopContainer>
        <S.ImgTop source={logo} />
        <Welcome />
      </S.TopContainer>
      <S.Title>{`No total temos ${data.length} consultas agendadas`}</S.Title>
      {isloading ? (
        <ActivityIndicator size="large" color="#3CB4E7" />
      ) : (
        <S.ContainerConsultas>
          {data.map(item => (
            <Consultas key={item.id} item={item} />
          ))}
        </S.ContainerConsultas>
      )}
    </S.Container>
  );
};
