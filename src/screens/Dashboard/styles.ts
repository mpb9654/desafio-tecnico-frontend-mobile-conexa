import styled from 'styled-components/native';
import { LinearGradient } from 'expo-linear-gradient';

export const Container = styled.View`
  flex: 1;
  background: ${({ theme }) => theme.colors.background_primary};
`;
export const ContainerConsultas = styled.ScrollView``;
export const Title = styled.Text`
  font-size: 16px;
  margin-top: 81px;
  margin-left: 24px;
  font-weight: 700;
`;
export const TopContainer = styled(LinearGradient).attrs(({ theme }) => ({
  colors: ['#7671E5', '#3CB4E7'],
  start: { x: 0, y: 0.7 },
  end: { x: 1.4, y: 0.4 },
}))`
  width: 100%;
  height: 175px;
  padding: 24px;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.primary};
`;
export const ImgTop = styled.Image`
  height: 36px;
  width: 190px;
  margin-top: 24px;
`;
