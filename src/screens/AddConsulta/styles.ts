import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: ${({ theme }) => theme.colors.background_primary};
`;
export const ContainerInputs = styled.ScrollView`
  padding: 0 24px;
`;
export const Title = styled.Text`
  font-size: 18px;
  margin-left: 24px;
  font-weight: 700;
`;
export const TopContainer = styled.View`
  width: 100%;
  padding: 24px;
  align-items: center;
`;
export const ImgTop = styled.Image`
  height: 36px;
  width: 190px;
  margin-top: 24px;
`;
export const Buttons = styled.View`
  padding: 24px;
  align-items: center;
  justify-content: center;
`;
