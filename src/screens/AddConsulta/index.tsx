import React, { useCallback, useEffect, useState } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useForm } from 'react-hook-form';
import { format } from 'date-fns';

import { ButtonCustomer } from '@components/ButtonCustomer';
import { DateText } from '@components/Inputs/InputDateTime';
import { InputNew } from '@components/Inputs/InputNew';
import { CONSULTA } from '@constants/endpoints/consulta';
import { api } from '@services/api';

import logo from '@assets/img/logo-conexa.png';

import * as S from './styles';

export const AddConsulta = () => {
  const navigation = useNavigation();
  const [date, setDate] = useState('');
  const [hour, setHour] = useState('');
  const [isloading, setIsLoading] = useState(false);
  const { control, handleSubmit } = useForm();

  const handleSaveConsult = useCallback(async form => {
    const year = Number(format(new Date(date), 'yyyy'));
    const month = Number(format(new Date(date), 'MM'));
    const day = Number(format(new Date(date), 'dd'));
    const hours = Number(format(new Date(hour), 'HH'));
    const minutes = Number(format(new Date(hour), 'mm'));
    const dateConsulta = new Date(year, month - 1, day, hours, minutes);

    const formData = {
      dataConsulta: dateConsulta,
      idMedico: 0,
      paciente: form.nomePaciente,
      observacao: form.observacao,
    };
    setIsLoading(true);
    try {
      const response = await api.post(CONSULTA, formData);
      if (response.status === 201) {
        if (Platform.OS == 'android') {
          ToastAndroid.show(
            'Nova consulta cadastrada com sucesso.',
            ToastAndroid.LONG,
          );
        }
        setIsLoading(false);
        navigation.goBack();
      }
    } catch (error: any) {
      if (Platform.OS == 'android') {
        ToastAndroid.show(error.message, ToastAndroid.LONG);
      }
    }
  }, []);

  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled
    >
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex: 1 }}
      >
        <S.Container>
          <S.TopContainer>
            <S.ImgTop source={logo} />
          </S.TopContainer>
          <S.Title>Dados para cadastrar nova consulta</S.Title>
          <S.ContainerInputs>
            <DateText
              title="Data da Consulta"
              value={date}
              setState={setDate}
              type="date"
            />
            <DateText
              title="Horário da Consulta"
              value={hour}
              setState={setHour}
              type="time"
            />
            <InputNew
              testID="nomePaciente"
              title="Nome paciente"
              name="nomePaciente"
              type="title"
              control={control}
              autoCapitalize="none"
              defaultValue=""
              autoCorrect={false}
            />
            <InputNew
              testID="observacao"
              title="Observação"
              name="observacao"
              type="description"
              control={control}
              autoCapitalize="none"
              defaultValue=""
              autoCorrect={false}
            />
          </S.ContainerInputs>
          <S.Buttons>
            <ButtonCustomer
              testID="Enviar"
              title="Enviar"
              onPress={handleSubmit(handleSaveConsult)}
              loading={isloading}
            />
          </S.Buttons>
        </S.Container>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};
