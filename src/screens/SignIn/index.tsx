import React from 'react';
import * as Yup from 'yup';
import { KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { useNavigation } from '@react-navigation/native';

import { InputCustomer } from '@components/Inputs/InputCustomer';
import { ButtonCustomer } from '@components/ButtonCustomer';
import { useAuth } from '@hooks/useAuth';

import logo from '@assets/img/logo-conexa.png';

import * as S from './styles';

const schema = Yup.object().shape({
  email: Yup.string().email().required('E-mail obrigatório'),
  password: Yup.string()
    .required('Senha obrigatória.')
    .min(8, 'Senha não pode ser menor que 8 caracteres.'),
});

export function SignIn() {
  const navigation = useNavigation();
  const { signIn } = useAuth();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const errorText = errors['password']?.message;
  const isError = Boolean(errorText);

  const handleSignIn = (form: any) => {
    signIn(form);
  };

  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled
    >
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex: 1 }}
      >
        <S.Container>
          <S.Logo>
            <S.ImgLogo source={logo} />
          </S.Logo>
          <S.Form>
            <S.Title>Faça seu login</S.Title>
            <S.Inputs>
              <InputCustomer
                testID="emailInput"
                name="email"
                control={control}
                iconName="user"
                autoCapitalize="none"
                defaultValue=""
                autoCorrect={false}
                placeholder="Seu e-mail"
                error={errors.email && errors.email.message}
              />
              <InputCustomer
                testID="passwordInput"
                name="password"
                control={control}
                iconName="lock"
                defaultValue=""
                secureTextEntry={true}
                placeholder="Sua senha"
                error={errors.password && errors.password.message}
              />
            </S.Inputs>
            {isError && <S.Title testID="nameErrorText">{errorText}</S.Title>}
            <S.Buttons>
              <ButtonCustomer
                testID="Entrar"
                title="Entrar"
                onPress={handleSubmit(handleSignIn)}
              />
            </S.Buttons>
          </S.Form>
        </S.Container>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
