import styled from 'styled-components/native';
import { LinearGradient } from 'expo-linear-gradient';

export const Container = styled(LinearGradient).attrs(({ theme }) => ({
  colors: ['#7671E5', '#3CB4E7'],
  start: { x: 1, y: 0.7 },
  end: { x: 1.4, y: 0.4 },
}))`
  flex: 1;
  justify-content: center;
`;
export const Logo = styled.View`
  align-items: center;
  justify-content: center;
`;
export const ImgLogo = styled.Image``;
export const Form = styled.View``;
export const Title = styled.Text`
  font-weight: 700;
  font-size: 21px;
  text-align: center;
  margin-top: 16px;
  color: ${({ theme }) => theme.colors.text};
`;
export const Inputs = styled.View`
  padding: 24px;
`;
export const Buttons = styled.View`
  padding: 24px;
  align-items: center;
  justify-content: center;
`;
