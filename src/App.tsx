import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { ThemeProvider } from 'styled-components';

import { AppProvider } from './context';
import theme from './styles/theme';
import { Routes } from './routes';

export default function App() {
  return (
    <NavigationContainer>
      <ThemeProvider theme={theme}>
        <AppProvider>
          <StatusBar backgroundColor="transparent" translucent />
          <View style={{ flex: 1 }}>
            <Routes />
          </View>
        </AppProvider>
      </ThemeProvider>
    </NavigationContainer>
  );
}
